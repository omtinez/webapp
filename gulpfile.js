var gulp = require('gulp');
var clean = require('gulp-clean');
var newer = require('gulp-newer');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-inline');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var transform = require('vinyl-transform');
var source = require('vinyl-source-stream');
var htmlhint = require("gulp-htmlhint");
var jshint = require("gulp-jshint");
var include = require('gulp-file-include')
var install = require('gulp-install')
var imagemin = require('gulp-imagemin');
var jimp = require('gulp-jimp');
var client = require('firebase-tools');
var mancha = require('gulp-mancha');
var ts = require('gulp-typescript');
var tap = require('gulp-tap');
var pckg = require('./package.json');

// Process arguments
var args = require('minimist')(process.argv.slice(1));
var env = args.env || 'development';


// Lint tasks

gulp.task('htmllint', function() {
    return gulp.src(['src/**/*.html', '!src/**/*.tpl.html'])
        .pipe(htmlhint())
        .pipe(htmlhint.reporter('fail'));
});

gulp.task('jslint', function() {
    return gulp.src(['src/**/*.js'])
        .pipe(jshint({ newcap: false, sub: true }))
        .pipe(jshint.reporter('fail'));
});

gulp.task('lint', gulp.series('jslint', 'htmllint'));


// Clean tasks

gulp.task('clean', function() {
    return gulp.src(['public/*'], { read: false })
        .pipe(clean());
});


// Build tasks

var imagesizes = [64, 128, 512];
gulp.task('logoresize', function() {
    return gulp.src('src/static/logo.png')
    .pipe(jimp(
        imagesizes.reduce(function(acc, size) {
            acc['-' + size] = {resize: {width: size}};
            return acc;
        }, {})
    ))
    .pipe(gulp.dest('public/static'));
});

gulp.task('imagemin', function () {
    return gulp.src(['src/**/*.png', 'src/**/*.jpg', 'src/**/*.jpeg', 'src/**/*.gif'])
        .pipe(imagemin())
        .pipe(gulp.dest('public'));
});

gulp.task('mancha', function () {
    return gulp.src(['src/**/*.html', '!src/**/*.tpl.html'])
        .pipe(mancha({
            env: env,
            name: pckg.displayName,
            description: pckg.description,
            year: new Date().getFullYear()
        }))
        .pipe(gulp.dest('public'));
});

gulp.task('copy', function () {
    return gulp.src(['src/**/*', '!src/**/*.ts', '!src/**/*.html'])
        .pipe(gulp.dest('public'));
});

gulp.task('ts', function() {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            target: 'ES2015',
            module: 'CommonJS',
            declaration: false,
            noImplicitAny: true,
         }))
        .pipe(gulp.dest('public'));
});

gulp.task('browserify', function () {
    return gulp.src(['public/**/*.js', '!public/node_modules/**/*'])
        .pipe(tap(function(file) {
            file.contents = browserify(file.path, {debug: true}).bundle();
        }))
        .pipe(buffer())
        .pipe(gulp.dest('public'));
});

gulp.task('uglify-js', function() {
    return gulp.src(['public/**/*.js', '!public/node_modules/**/*'])
        .pipe(uglify())
        .pipe(gulp.dest('public'));
});

gulp.task('minify-html', function() {
    return gulp.src(['public/**/*.html', '!public/node_modules/**/*'])
        .pipe(minify({jsSelector: 'script[data-do-not-minify!=true]'}))
        .pipe(gulp.dest('public'));
});

gulp.task('thirdparty', function () {
    return gulp.src(['package.json'])
        .pipe(gulp.dest('public'))
        .pipe(install({production: true}));
});


// Deploy tasks

gulp.task('firebase', function(callback) {
    return client.deploy({
        token: process.env.FIREBASE_TOKEN
    }).then(function() {
        return callback();
    }).catch(function(err) {
        console.log(err);
        return process.exit(1);
    });
});

// Used to make sure the process ends
gulp.task('exit', function(callback) {
    callback();
    process.exit(0);
});


// High level tasks

gulp.task('debug', gulp.series('lint', 'logoresize', 'mancha', 'copy', 'ts', 'browserify', 'thirdparty'));
gulp.task('build', gulp.series('debug', 'imagemin', 'uglify-js', 'minify-html'));
gulp.task('deploy', gulp.series('firebase', 'exit'));
