# Web App Template
A simple template for web apps built on top of [UIToolkit](https://gitlab.com/omtinez/uitoolkit)

## Usage
Clone this project and replace all references of {{project_name}} and {{project_description}}. Don't forget to update this README too!
